// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include <iostream>
#include <cassert>
#include <random>

#include "Generator.h"
#include "Pipe.h"

using namespace std;

namespace
{
    std::default_random_engine gen{};

    // create a random distribution between 1..3
    std::uniform_int_distribution random_alarm_type{
        static_cast<int>(Alarm::Type::advisory),
        static_cast<int>(Alarm::Type::warning)};

    Alarm make_random_alarm()
    {
        return Alarm{static_cast<Alarm::Type>(random_alarm_type(gen))};
    }
}

Generator::Generator(Pipe &pipe) : output{pipe} {}

void Generator::run()
{
    cout << "GENERATOR : ----------------------------------\n";

    auto  alarm = make_random_alarm();
    cout << alarm << '\n';
    output.push(alarm);

    cout << '\n';
}
