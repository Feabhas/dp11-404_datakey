
#include "Alarm_filter.h"
#include "Display.h"
#include "Generator.h"
#include "Pipe.h"
#include "Pipeline.h"
#include <chrono>
#include <thread>

using namespace std::chrono_literals;

bool done{};

int main(int argc, char** argv) {
  const int run_count = (argc > 1) ? std::stoi(argv[1]) : 1;

  Pipe pipe1{};
  Pipe pipe2{};

  Generator generator{pipe1};
  Display display{pipe2};
  Alarm_filter filter{Alarm::advisory, pipe1, pipe2};

  auto run_n_times = [](int num_times, Filter& filter) {
    for (int i{0}; i < num_times; ++i) {
      filter.run();
      std::this_thread::sleep_for(1000ms);
    }
    done = true;
  };

  auto run_forever = [](Filter& filter) {
    while (!done) {
      filter.run();
      std::this_thread::yield();
    }
  };

  std::thread gen_thread{run_n_times, run_count, std::ref(generator)};
  std::thread filter_thread{run_forever, std::ref(filter)};
  std::thread display_thread{run_forever, std::ref(display)};

  // Wait for the threads to
  // finish
  //
  gen_thread.join();
  filter_thread.join();
  display_thread.join();
}