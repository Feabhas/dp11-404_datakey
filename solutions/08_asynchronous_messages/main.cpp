
#include <chrono>
#include <thread>
#include "Generator.h"
#include "Display.h"
#include "Alarm_filter.h"
#include "Pipe.h"
#include "Pipeline.h"
#include "Thread_proxy.h"


using namespace std;
using namespace chrono_literals;

// Flag to allow the Generator thread
// to force the other threads to terminate
//
bool done { false };


int main()
{
    Generator    generator { };
    Display      display   { };
    Alarm_filter filter    { Alarm::advisory };
    

    connect(generator, filter);
    connect(filter, display);

    auto run_n_times =  [](int num_times, Filter& filter) 
                        {
                            for (int i { 0 }; i < num_times; ++i) {
                                filter.run();
                                this_thread::sleep_for(1000ms);
                            }
                            done = true;
                        };

    auto run_forever =  [](Filter& filter)
                        {
                            while (!done) {
                                filter.run();
                                this_thread::yield();
                            }
                        };

    Thread gen_thread     { "Generator", run_n_times, 5, ref(generator) };
    Thread filter_thread  { "ID Filter", run_forever, ref(filter) };
    Thread display_thread { "Display",   run_forever, ref(display) };

    // Wait for the threads to
    // finish
    //
    gen_thread.join();

    // Since the downstream threads are blocked
    // on their pipes, don't wait for them to 
    // return (because they won't), just terminate
    // them preemptively.  This is not the ideal
    // solution, but acceptable for this simple
    // exercise.
    //
    // filter_thread.join();
    // display_thread.join();
}