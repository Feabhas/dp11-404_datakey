// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#ifndef ALARM_FILTER_H_
#define ALARM_FILTER_H_

#include <functional>
#include "Filter.h"
#include "Alarm.h"
#include "Threadsafe_queue.h"



class Alarm_filter : public Filter {
public:
    Alarm_filter(Alarm::Type remove_this);

    void run()                           override;
    void update(const Alarm_list& param) override;
    void update(Alarm_list&& param)      override;

private:
    Alarm::Type value;

    Filter* downstream { nullptr };
    friend void connect(Alarm_filter& filter, Filter& ds);

    using Impl_fn       = std::function<void()>;
    using Message_queue = Threadsafe_queue<Impl_fn>;

    Message_queue queue { };

    void update_impl(Alarm_list& alarms);
};

#endif
