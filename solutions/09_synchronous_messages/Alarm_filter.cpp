// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include <algorithm>
#include <cassert>
#include <functional>
#include "Alarm_filter.h"
#include "Pipe.h"

using namespace std;

Alarm_filter::Alarm_filter(Alarm::Type remove_this) :
    value { remove_this }
{
}


void Alarm_filter::run()
{
    auto task = queue.pop();
    task();
}


Alarm_list Alarm_filter::get()
{
    Sync_task task { bind(&Alarm_filter::get_impl, this) };
    auto return_val = task.get_future();
    queue.push(move(task));
    return return_val.get();
}


Alarm_list Alarm_filter::get_impl()
{
    assert(upstream);
    
    auto alarms = upstream->get();

    cout << "ALARM FILTER : -------------------------------" << endl;

    auto original_size = alarms.size();

    auto it = remove_if(
        begin(alarms), 
        end(alarms), 
        [this](const Alarm& alarm) { return alarm.type() == value; }
    );
    alarms.erase(it, end(alarms));
    
    auto elements_removed = original_size - alarms.size();
    cout << "Removing " << elements_removed;
    cout << " alarm" << (elements_removed != 1 ? "s" : "");
    cout << endl;

    cout << endl;

    return alarms;
}


void connect(Alarm_filter& filter, Filter& us)
{
    filter.upstream = &us;
}