#ifndef THREADSAFE_QUEUE_H_
#define THREADSAFE_QUEUE_H_

#include <queue>
#include <mutex>
#include <condition_variable>


template <typename T>
class Threadsafe_queue : public std::queue<T> {
public:
    // Perfect forwarding for push()
    // 
    template <typename U>
    void push(U&& in);

    T pop();
    bool empty() const;

private:
    mutable std::mutex mtx { };
    std::condition_variable not_empty { };
};


template <typename T>
template <typename U>
void Threadsafe_queue<T>::push(U&& in)
{
    std::lock_guard lock { mtx };

    std::queue<T>::push(std::forward<U>(in));
    not_empty.notify_one();
}


template <typename T>
T Threadsafe_queue<T>::pop()
{
    std::unique_lock lock { mtx };

    while (std::queue<T>::empty()) {
        not_empty.wait(lock);
    }

    auto return_val = std::move(std::queue<T>::front());
    std::queue<T>::pop();
    return return_val;
}


template <typename T>
bool Threadsafe_queue<T>::empty() const
{
    std::lock_guard lock { mtx };
    return std::queue<T>::empty();
}


#endif