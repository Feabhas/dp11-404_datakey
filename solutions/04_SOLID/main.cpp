// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Alarm_filter.h"
#include "Display.h"
#include "Generator.h"
#include "Pipe.h"
#include "Pipeline.h"

int main(int argc, char** argv) {
  const int run_count = (argc > 1) ? std::stoi(argv[1]) : 1;

  Pipe pipe1{};
  Pipe pipe2{};

  Generator generator{pipe1};
  Display display{pipe2};
  Alarm_filter filter{Alarm::Type::advisory, pipe1, pipe2};

  // Pipeline pipeline{};
  // pipeline.add(generator);
  // pipeline.add(filter);
  // pipeline.add(display);

  Pipeline pipeline{&generator, &filter, &display};

  for (int i{}; i < run_count; ++i) {
    pipeline.run();
  }
}
