// -----------------------------------------------------------------------------
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any
// warranty of the item whatsoever, whether express, implied, or statutory,
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including,
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether
// or not based upon warranty, contract, tort, or otherwise; whether or not
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include "Alarm_filter.h"
#include "Pipe.h"
#include <algorithm>
#include <cassert>

using namespace std;

Alarm_filter::Alarm_filter(Alarm::Type remove_this, Pipe& in, Pipe& out)
    : value{remove_this}, input{in}, output{out} {}

void Alarm_filter::run() {
  if (input.is_empty())
    return;

  cout << "ALARM FILTER : -------------------------------\n";

  auto alarms = input.pull();

#if __cplusplus <= 201710L
  //     // Modern C++
  auto it = remove_if(begin(alarms), end(alarms), [this](const Alarm& alarm) {
    return alarm.type() == value;
  });

#else
  // C++20 Requires g++ >= 10
  auto [it, End] = std::ranges::remove_if(
      alarms, [this](const Alarm& alarm) { return alarm.type() == value; });

#endif

  if (auto elements_removed = std::distance(it, std::end(alarms));
      elements_removed > 0) {
    cout << "Removing " << elements_removed;
    cout << " alarm" << (elements_removed != 1 ? "s\n" : "\n");
  }

  alarms.erase(it, end(alarms));
  // alarms.erase(it, End);   // C++20 ranges

  output.push(alarms);
  cout << '\n';
}
