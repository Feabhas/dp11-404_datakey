DP11-404 Code Project Files
=============================
This code has currently been tested with

* g++ 8.1.0
* scons 2.4.1


DOWNLOADING THE EXERCISE TEMPLATE/SOLUTIONS
===========================================
You should be able to install the project templates
in any directory of your choice. (In this example, we
have used the user's home directory (~)

```
$ cd ~
$ git clone https://bitbucket.org/Feabhas/dp11-404_datakey.git
$ cd dp11-404_datakey
```

The repository structure
------------------------
The top-level structure is as follows
```

├── README.md      - This file
├── docs           - Exercise hints-and-tips
├── solutions      - Solutions code
└── template       - Exercise template
```

THE EXERCISE TEMPLATE IS OPTIONAL
---------------------------------
You are not required to use the supplied exercise
template for this training course.  It is provided
for your convenience only.

The primary requirement for the course exercises are
the capability for you to build a simple "Hello World!"
console application.

It does not matter significantly what development
environment you choose for the exercises.  In the past
delegates have used:

* vi/g++/make/gdb (Linux)
* Xcode 	  (Mac)
* Visual Studio   (Windows)

As this course focuses on Modern C++, your toolchain must
support (at least) the C++17 standard.


USING THE EXERCISE TEMPLATE
===========================
The exercise template is convenient if you are using a 
Linux or Mac environment; or a Linux subsystem on Windows
The following information will help you navigate and use
the template.


Template structure
------------------
The exercise template is organised as follows
```
template
├── build                        - Build system files
│   ├── CMake                    - CMake configuration
│   ├── SCons                    - SCons configuration
│   └── bin                      - Executable file
│
├── build-ex-cmake.sh            - Solution build scripts 
├── build-ex-scons.sh              (see below for details)
├── init_src
│ 
├── src                          - Your source code goes here
│   ├── SConscript               - SCons config (do not delete)
│   └── main.cpp
│
├── DP11-404.code-workspace    - VSCode config
├── DP11-404.sublime-project   - Sublime Text config
├── .vscode   
```

Choosing a build system
-----------------------
The template project is set up for two build systems
* SCons
* CMake

SCons:
SCons is a Python-based build system.  A basic tutorial
for setting it up and using it can be found here:

https://riptutorial.com/scons/topic/9377/getting-scons-running


CMake
CMake is a very well known cross-platform build management
tool. A tutorial for installing and getting started with
CMake can be found here:

https://riptutorial.com/cmake

You do have to install both build systems - either option will
work.

You do not have to configure your own CMakeLists.txt or SConstruct
file for the project, we have supplied working versions in the project
template.

BUILDING THE TEMPLATE PROJECT
=============================

Building the exercise template with Scons
-----------------------------------------
```
$ cd template
$ scons -f build.script/SCons/SConstruct     # build
$ ./build/bin/DP11-404                       # run
$ scons -f build.script/SCons/SConstruct -c  # clean
```

Building the exercise template with CMake
-----------------------------------------
```
$ cd template
$ cmake -S . -B build                      # don't forget the '.'
$ cmake --build build/
$ ./build/bin/DP11-404                     # run
$ cmake --build build --  clean            # to clean the build
```

NOTE:
Adding a new source file to your project does
NOT add it to the CMakeList configuration.
Therefore you will need to rerun cmake whenever
you add a new file.
You need to only run `cmake --build build` each time you edit a
file.


EXERCISE SOLUTIONS
==================
We have provided full working exercise solutions
Bringing an exercise solution into the
project template.  xx is the exercise
number. Using 0 as the exercise number will
reset the project

Using SCons:
```
$ cd template
$ ./build-ex-scons.sh xx
```

Using CMake:
```
$ cd template
$ ./build-ex-cmake.sh xx
```

You may need to grant execute permission
on the script file to run it; for example:

```
$ chmod 777 build-ex-cmake.sh
$ chmod 777 build-ex-scons.sh
```


Text editor choices
-------------------
We do not constrain your choice of text editor.  Most
will use their IDE's editor.  However, stand-alone text
editors are becoming far more popular choices.  To that
end the project templates included project/workspace
configurations for two of the more popular text editors
- Visual Studio Code and Sublime Text.

The editor projects have build tasks set up for both
SCons and/or CMake, allowing you to build (using our
build scripts) directly from the editor.

NOTE:
For VS Code the 'make' build option will also run cmake,
including any newly-added files into your project.
For Sublime Text you will need to manually run cmake each
time you add a new file to the project.